(function(){
	svg4everybody();
	$('[data-toggle="toggleVisible"]').click(function() {
		$( '[data-id="'+ $(this).data('target') +'"]' ).toggleClass('visible');
		if ( $(this).data('target') == 'menu' ) {
			if ( !$('[data-id="'+$(this).data('target')+'"] li').hasClass('animated') ) {
				$('[data-id="'+$(this).data('target')+'"] li').addClass('zoomIn faster animated')
			} else {
				$('[data-id="'+$(this).data('target')+'"] li').removeClass('zoomIn faster animated')
			}
		}
	});
	function checkLazyManyItems(e) {
		var slides = e.relatedTarget.$stage.children();
		[].forEach.call(slides, function(elem) {
			if ( elem.classList.contains('active') ) {
				var lazyImgs = elem.querySelectorAll('.lazy-img');
				[].forEach.call(lazyImgs, function(elems) {
					if ( !elems.classList.contains('lazyload') ) {
						elems.classList.add('lazyload')
					}
				})
			}
		})
	};
	function addClassToLastItems(e) {
		var total = $(e.target).find('.owl-item.active').length;
			console.log(total);

			$(e.target).find('.owl-item').removeClass('last');

			$(e.target).find('.owl-item.active').each(function(index){
				if (index === total - 1 && total>1) {
					$(this).addClass('last');
				}
			});
	}
	$('.portfolio-slider').owlCarousel({
		items: 4,
		margin: 120,
		nav: true,
		autoplay: true,
		navText: ['<svg class="svg-icon">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow2"></use>'+
				'</svg>',
				'<svg class="svg-icon">'+
					'<use xlink:href="images/svg-sprite/svgSprite.svg#arrow2"></use>'+
				'</svg>',],
		dots: false,
		responsive: {
			0: {
				items: 1,
				margin: 15
			},
			576: {
				items: 2,
				margin: 30
			},
			992: {
				items: 3,
				margin: 30
			},
			1300: {
				items: 4,
				margin: 120
			},
		},
		onInitialized: function(e) {
			addClassToLastItems(e);
			checkLazyManyItems(e);
		},
		onTranslated: function(e) {
			addClassToLastItems(e);
			checkLazyManyItems(e);
		},

	});
	$('.testimonial-carousel').owlCarousel({
		items: 2,
		margin: 30,
		loop: true,
		nav: false,
		autoplay: true,
		dots: true,
		responsive: {
			0: {
				items: 1,
				margin: 15
			},
			576: {
				items: 2,
				margin: 30
			},
		}
	});
	$('.pseudo-select-self').click(function() {
		console.log('alalalal')
		$(this).toggleClass('open');
		if ( $(this).hasClass('open') ) {
			console.log('lalalal')
			$(this).siblings('.svg-icon').addClass('rotate-negative-90');
		} else {
			$(this).siblings('.svg-icon').removeClass('rotate-negative-90');
		}
	})
	new WOW().init();
}());