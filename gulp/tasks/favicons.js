module.exports = function() {
	$.gulp.task("favicons", async function(done) {
		return $.gulp.src("./app/favicons/*.{jpg,jpeg,png,gif}")
			.pipe($.gp.favicons({
				icons: {
					appleIcon: false,
					favicons: true,
					online: false,
					appleStartup: false,
					android: false,
					firefox: false,
					yandex: false,
					windows: false,
					coast: false
				}
			}))
			.pipe($.gulp.dest("./build/"))
			.on('end', done);
	});
};